const http = require("http");

const rabbit = require("amqplib");
(async () => {
	try {
		const connection = await rabbit.connect("amqp://rabbit");
		const channel = await connection.createChannel();
		await channel.assertQueue("queue", { durable: true });

		channel.consume(
			"queue",
			(msg) => {
				return http
					.request(
						{
							method: "POST",
							hostname: "api",
							port: 3000,
							path: "/",
							headers: {
								"Content-Type": "application.json",
							},
						},
						(res) =>
							res
								.on("data", (buff) => {
									if (res.statusCode === 429) {
										console.log(
											`Send again to queue by [Status 429] after(ms): ${
												res.headers["retry-after"] *
												1000
											}`
										);
										setTimeout(() => {
											channel.sendToQueue(
												"queue",
												Buffer.from(msg.content)
											);
											channel.ack(msg);
										}, res.headers["retry-after"] * 1000);
									} else {
										console.log(buff.toString("utf-8"));
										channel.ack(msg);
									}
								})
								.on("error", (err) => {
									console.error("Error: ", err);
									console.log("Send again to the queue");
									channel.sendToQueue(
										"queue",
										Buffer.from(msg.content)
									);
									channel.ack(msg);
								})
					)
					.write(msg.content.toString("utf-8"));
			},
			{ noAck: false }
		);
	} catch (e) {
		console.error("E:", e);
		process.exit(1);
	}
})();
