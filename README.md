# Respuesta al ejercicio

_Nota_: A la hora de hacer pruebas la `api` me fallaba por un error en la librería `body-parser`. Para comprobar que mi código funcionaba correctamente, he hecho pruebas contra otras api's similares, y he modificado la original. Pero como en el ejercicio se pedía solo modificar el `sub` he preferido revertir los cambios y dejar esta parte como al principio.

El principal problema que me he encontrado en el ejercicio ha sido gestionar los 429 al enviar tantas peticiones. Para solucionarlo, cuando me llega una petición con `statusCode === 429`, espero el tiempo que me indica la cabecera `retry-after` y seguidamente vuelvo a encolar esa llamada.
Si se ha producido un error en la petición también vuelvo a añadir a la cola el contenido del mensaje.

Aparte he añadido la configuración de `{ noAck: false }` para que si se corta algún proceso `sub` pueda retomarlo otro y evitar pérdidas.
