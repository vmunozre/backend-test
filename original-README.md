# Backend
There's a `pub`lisher continuously sending events to a (`rabbitmq`) queue which
then get processed by a `sub`scriber which sends them to a Rate-Limit(ed) `api`.

The rate at which the events happens is faster than the rate-limit and
*some requests are being rejected*.

*Find a solution* so that all events get sent to the API *with no messages lost*
and without altering either the `api` nor the `pub`lisher.

The solution must scale to *any number* of *concurrent* `sub` instances:

```
docker-compose up
docker-compose scale sub=3
```
